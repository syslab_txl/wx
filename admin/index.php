<?php

//define('APP_DEBUG',TRUE);   //开启调试模式
define('THINK_PATH', "../Core/");
define('APP_NAME', "admin");
define('APP_PATH', "../admin/");
define('HOME_PATH', '../Apps/');
define('M_PATH', 'm/');

$path_arr = explode("/",$_SERVER["SCRIPT_NAME"]);
$main_root = "";
for($i=0;$i<count($path_arr)-2;$i++)
	if(!empty($path_arr[$i]))
		$main_root .= "/".$path_arr[$i];
define('MAIN_ROOT_WHY',$main_root);

require THINK_PATH.'ThinkPHP.php';
