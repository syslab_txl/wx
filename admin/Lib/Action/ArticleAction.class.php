<?php

class ArticleAction extends CommonAction{
    
	//加为首页轮播
	function addTopic()
	{
		if(isset($_REQUEST["id"]))
		{
			$id = $_REQUEST["id"];
			$order = explode(",",$_REQUEST["order"]);
			$name = $this->getActionName();
			$model = D($name);
			$slideModel = D("Slide");
			$condition = array("id" => array('in', explode(',', $id)));
            $list = $model->where($condition)->select();
			$arr = array();
			foreach($list as $key=>$value)
			{
				$arr["title"] = $value["title"];
				$arr["img"] = $value["thumb"];
				$arr["url"] = MAIN_ROOT_WHY."/index.php/article/show/id/".$value["id"];
				$arr["listorder"] = $order[$key];
				$arr["status"] = 1;
				//var_dump($arr);
				$slideModel->add($arr);
			}
			
			header("location:".__APP__."/slide");
		}
		else
		{
			$map = $this->_search();
			if (method_exists($this, '_filter')) {
				$this->_filter($map);
			}
			$name = $this->getActionName();

			$model = D($name);
			if (!empty($model)) {
			
				$this->_list($model, $map);
			}
			$this->display();
		}
	}
	
    //过滤查询字段
    function _filter(&$map){
        if(isset($_GET['catid'])){
            $map['catid']=  array('eq',$_GET['catid']);
        }
        if(isset($_POST['catid'])){
            $map['catid']=  array('eq',$_POST['catid']);
        }
        if(!empty($_POST['name'])){
            $map['title'] = array('like',"%".$_POST['name']."%");
        }
        
        //状态
        if(isset($_POST['zt'])&&$_POST['zt']!=-2){
            $map['status'] = array('eq',$_POST['zt']);
            $this->zt=$_POST['zt'];
        }
    }
    public function _before_index() {
        if(isset($_GET['catid'])){
            $this->catid=$_GET['catid'];
        }
        if(isset($_POST['catid'])){
            $this->catid=$_POST['catid'];
        }
    }
    public function _before_add() {
        if(isset($_GET['id'])){
            $this->catid=$_GET['id'];
        }
    }
    public function _before_edit() {

        $cate=new CategoryModel();
        $menu =$cate->getModelCategory('Article'); //加载栏目
        $menu1 = arrToMenu($menu,0);  
        $this->categorylist=$menu1;

    }
   

}

?>
