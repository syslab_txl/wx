<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <title></title>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel='stylesheet' type="text/css" href="../Public/css/style.css" />
        <script type="text/javascript" src="../Public/js/jquery-1.7.2.min.js"></script>
        <script type="text/javascript" src="../Public/js/common.js"></script>
        <script type="text/javascript" src="../Public/js/jquery-yufu5.js"></script>
        
        <script type="text/javascript">
            $(function(){
                if($.browser.msie&&$.browser.version=="6.0"&&$("html")[0].scrollHeight>$("html").height())
                    $("html").css("overflowY","scroll");
            });
        </script>
        <script language="JavaScript">
        <!--
        //指定当前组模块URL地址 
        var URL = '__URL__';
        var APP	 = '__APP__';
        var SELF='__SELF__';
        var PUBLIC='__PUBLIC__';
        var Public = '../Public/';
        //-->
        </script>
        <script type="text/javascript" src="../Public/js/iColorPicker.js"></script>
        <script type="text/javascript" src="../Public/ueditor/editor_config.js"></script>
        <script type="text/javascript" src="../Public/ueditor/editor_all.js"></script>
    </head>
    <body>
<div class="main">
    <div class="box_tit">
        <h2>帐号推荐设置</h2>
    </div>
    <div class="operate">
        <script type="text/javascript">
            var addurl="<?php echo U('Weixin/addtj');?>";
        </script>
        <div class="fLeft">
            <form id="form1" name="form1" method="post" action="<?php echo U('Weixin/hot');?>">
                <input type="text" name="name" title="请输入关键字(公众帐号)" class="ipt5">
                <select name="tj">
                    <option value="-2" <?php if(($tj) == "-2"): ?>selected="selected"<?php endif; ?>>全部</option>
                    <option value="1" <?php if(($tj) == "1"): ?>selected="selected"<?php endif; ?>>已推荐</option>
                    <option value="0" <?php if(($tj) == "0"): ?>selected="selected"<?php endif; ?>>未推荐</option>
                </select>
                <input type="submit" class="submit btn5" value="查  询">
            </form>
        </div>
        <?php if(!empty($ISTASK)): ?><div class="fLeft">
            <form id="form2" name="form2"  method="post" action="<?php echo U('Weixin/tasktj');?>">
                <input type="text" name="taskmark" title="请输入任务标识码" class="ipt5" style="margin-left: 20px;">
                <input type="submit" class="submit btn5" value="任务提交">
            </form>
        </div><?php endif; ?>
    </div>
    <div class="list">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tbody>
            <tr class="nbg">
                <th width="40"><input type="checkbox" id="check"></th>
                <th>编号</th>
                <th>公众账号</th>
                <th>所属分类</th>
                <th>类型</th>
                <th>关注度</th>
                <th>会员名</th>
                <th>发布时间</th>
                <th>操作</th>
            </tr>
            <?php if(is_array($list)): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><tr>
                <td><input type="checkbox" name="key" value="<?php echo ($vo["id"]); ?>"></td>
                <td><?php echo ($vo["id"]); ?></td>
                <td><?php echo ($vo["pubaccount"]); ?></td>
                <td><?php echo (getcategoryname($vo["catid"])); ?></td>
                <td><?=$vo["typeid"]==1?"订阅号":"服务号";?></td>
                <td><?php echo ($vo["hits"]); ?></td>
                <td><?php echo ($vo["membername"]); ?></td>
                <td><?php echo (todate($vo["create_time"],"Y-m-d H:i")); ?></td>
                <td>
                    <a href="<?php echo U('Weixin/addtj',array('id'=>$vo['id']));?>">推荐</a>
                </td>    
            </tr><?php endforeach; endif; else: echo "" ;endif; ?>
            </tbody>
        </table>
        <div class="th" style="clear: both;"><?php echo ($page); ?></div>
    </div>
</div>

    </body>
</html>