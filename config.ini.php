<?php
if(!defined('THINK_PATH')) exit();
return $array = array (
  'DB_TYPE' => 'mysql',
  'DB_HOST' => 'localhost',
  'DB_NAME' => 'erweima',
  'DB_USER' => 'root',
  'DB_PWD' => '123',
  'DB_PORT' => 3306,
  'DB_PREFIX' => 'wx_',
  'DB_BACKUP' => '../Data/backup',
  'RBAC_ROLE_TABLE' => 'wx_role',
  'RBAC_USER_TABLE' => 'wx_user',
  'RBAC_ACCESS_TABLE' => 'wx_access',
  'RBAC_NODE_TABLE' => 'wx_node',
  'URL_CASE_INSENSITIVE' => true,
  'URL_MODEL' => 2,
  'DEFAULT_THEME' => 'default',
  'WX_TOKEN' => 'yf519ee20672526684232448',
  'SITE_NAME' => '二维码推荐',
  'SITE_TITLE' => '微信公众平台帐号导航',
  'SITE_KEYWORDS' => '微信 公众平台 帐号导航',
  'SITE_DESCRIPTION' => '微信公众平台帐号导航',
  'RMB_CHANGE_INTERGRAL' => 20,
  'ISCOMMENT' => 1,
  'COMMENTTYPE' => 1,
  'ISADDACCOUNT' => 1,
  'XFINTERGRAL' => 10,
  'ISAUTOVERIFY' => 0,
  'YUFUMARK' => '',
  'ISTASK' => 0,
  'TASKURL' => '',
  'URL_PATHINFO_DEPR' => '/',
  'TMPL_PARSE_STRING'  =>array(
            '__APP__' => "http://".$_SERVER["HTTP_HOST"],
        )
);
?>