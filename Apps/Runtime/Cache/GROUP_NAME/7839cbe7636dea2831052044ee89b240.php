<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<!-- saved from url=(0051)http://www.zhaogonghao.com/statics/images/focus.htm -->
<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<title>焦点幻灯</title>
<style type="text/css"> 
<!--
/*全局样式*/
body,ul,ol,dl,dt,dd,li,p,h1,h2,h3,h4,h5,h6,form,fieldset,table,td,img,div{margin:0;padding:0;border:0;}
body,ul,ol,li,p,form,fieldset,table,td{font-family:"微软雅黑"; font-weight:normal;}
body{color:#ebebeb; background:#ebebeb;}
body{margin-top:0px; background:#fff url(../source/bg.png) repeat-y center;}
td,p,li,select,input,textarea,div{font-size:12px; text-align:left; color: #000; line-height: 24px;}
ul{list-style-type:none; color: #000000;}
ol{list-style-type:none; color: #000000;}
select,input{vertical-align:middle;}
a{text-decoration:none;}
a:link{color:#272727; text-decoration:none}
a:visited{color:#800080;text-decoration: none;}
a:hover,a:active,a:focus{color:#c00; text-decoration:underline;}
.clearit{clear:both;}
a:focus {outline:none; -moz-outline:none;}
.focus {width:980px; height:595px; margin:0px auto; margin-top:10px;}
.box335-2 {background: none repeat scroll 0 0 #FFFFFF;  width: 690px; height:260px; float:left;}
.fPic03 {height: 260px;  position: relative;  width: 690px;}
.bigPic03 {position: absolute;}
.dotList12 {position: absolute; right: 3px; top: 243px;}
.dotList12 li {background: url("../Public/source/i_dot.gif") no-repeat scroll right top transparent; cursor: pointer;  display: inline; float: left; height: 7px;  margin: 0 7px 0 0; width: 7px;}
.textbg { background: none repeat scroll 0 0 #000000; height: 60px; left: 0;  filter:alpha(opacity=50); -moz-opacity:0.5; -khtml-opacity: 0.5; opacity: 0.5; position: absolute; top: 200px; width: 690px;}
.text12 {height: 60px; left: 10px;  line-height: 60px; position: absolute; top: 200px; width: 680px;}
.dotList12 li.current {background: url("../Public/source/i_dot.gif") no-repeat scroll left top transparent;}
#prevSlide, #nextSlide {cursor: pointer; height: 34px; left: 0; position: absolute; text-align: center; top: 100px; width: 35px;  z-index: 10;  filter:alpha(opacity=60); -moz-opacity:0.6; -khtml-opacity: 0.6; opacity: 0.6;}
#prevSlide img, #nextSlide img {left: 12px; position: absolute; top: 9px; z-index: 12;  background:none;border:none;padding:0px;
}
.btnBg {background: none repeat scroll 0 0 #000000; height: 34px; left: 0; opacity: 0.6; position: absolute; top: 0; width: 35px; z-index: 11;}
#nextSlide {left: 655px;}
.cWhite a {color:#ffffff;font-weight: bold; font-size:24px;text-decoration:none;}  
.cWhite a:hover {color:#ffffff;font-weight: bold; font-size:24px;text-decoration:none;}  
ul{margin:0px;padding:0px} 
li{margin:0px;padding:0px}
h3{margin:0px;padding:0px} 
.bigPic03 a img{border:none;padding:0px}
-->
</style> 
<script type="text/javascript" src="../Public/js/jquery-1.7.2.min.js"></script>
<script src="../Public/js/BX.js" type="text/javascript"></script> 
<script src="../Public/js/TabControl.js" type="text/javascript"></script> 
</head>

<body marginwidth="0" marginheight="0">

<div class="box335-2">
<div id="tabSlide" class="fPic03">
<?php $num=0; ?>
  <?php $_result=M('Slide')->where('status=1')->field('id,title,url,img')->order('listorder desc')->limit(5)->select();foreach($_result as $key=>$slide): $num++;?>
    <div class="bigPic03" style="display: <?=$num==1?"":"none"?>">
      <a href="<?php echo ($slide["url"]); ?>" target="_blank"><img src="<?=MAIN_ROOT_WHY?>/Uploads<?php echo ($slide["img"]); ?>" alt="<?php echo ($slide["title"]); ?>" height="260" width="690"></a> 
      <!--<div class="textbg"></div>
      <div class="text12">
        <h3 class="cWhite"><a href="<?php echo ($slide["url"]); ?>" target="_blank"><?php echo ($slide["title"]); ?></a></h3>
      </div>-->
    </div><?php endforeach;?>
  <ul id="controlSlide" class="dotList12">
    <?php for($i=0;$i<$num;$i++){ if($i==0){ ?>
        <li class="current"></li>
    <?php }else{ ?>
        <li class=""></li>
    <?php }} ?>
  </ul>

<div id="prevSlide" class="unenable"><a><img src="../Public/source/N_left.gif" height="16" width="8"></a> 
<div class="btnBg" style="filter:alpha(opacity=60);-moz-opacity:0.6;opacity:0.6;"></div></div>
<div id="nextSlide" class="enable"><a><img src="../Public/source/N_right.gif" height="16" width="8"></a> 
<div class="btnBg" style="filter:alpha(opacity=60);-moz-opacity:0.6;opacity:0.6;"></div></div></div></div>

<script type="text/javascript"> 
function Collection(){
  this.items=[];
}
Collection.prototype={
  add:function(col){
    this.items.push(col);
  },
  clear:function(){
    this.items=[];
  },
  getCount:function(){
    return this.items.length;
  },
  each:function(func){
    for(var i=0;i<this.getCount();i++){
      func(this.items[i]);
    }
  },
  indexOf:function(item){
    var r=-1;
    for(i=0;i<this.getCount();i++ ){
      if(item==this.items[i]){ r=i; break;}
    }
    return r;
  },
  find:function(func){
    var r=null;
    for(var i=0;i<this.getCount();i++){
      if(func(this.items[i])==true){ r=this.items[i];break;}
    }
    return r;
  },
  findAll:function(func){
    var r=new Collection();
    this.each(
      function(item){ 
        if(func(item)==true){ r.add(item); }
      }
    );
    return r;
  }
}
function TabPage(triggerId,sheetId){
  this.trigger=$(triggerId);
  this.sheet=$(sheetId);
}
 
 
function TabControl(){
  this.styleName=null;
  this.tabPages=new Collection();
  this.currentTabPage=null;
  this.triggerType='click';
  this.defaultPage=0;
  this.enableSlide=false;
  this.slideInterval=3000;
  this.autoLoop=false;//xingming add20110531
  this.preButton=null;
  this.nextButton=null;
  this.onComplete = null;
  this.options = null;
  this.currentClassName ='current';
  this.onChanging=new Collection();
  /*添加默认事件处理句柄*/
 
  this.onChanging.add( this.defaultChangingHandler );
  this.onInit=new Collection();
 
  /*添加默认初始化句柄*/
 
  this.onInit.add(this.defaultInitHandler);
  this.onInit.add(this.autoSlideInitHandler);
  this.onAdding=new Collection();
 
  /*标签页添加事件处理*/
  this.onAdding.add( this.defaultAddingHandler );
  /*private*/
 
  this._autoSlideEv=null;
}
TabControl.prototype={
  add:function(tabPage){
    this.tabPages.add(tabPage);    
    var handler=function(func){ func(tabPage); };
    this.onAdding.each( handler );
  },
  addRange:function(triggers,sheets){

    if(triggers.length==0||triggers.length!=sheets.length){ return; }
    for(var i=0;i<triggers.length;i++){
      var tabPage= new TabPage(triggers[i],sheets[i]);
      this.add(tabPage);
    }
  },
 
  pre:function(),
  next:function(),
  defaultAddingHandler:function(tabPage){
  },
  init:function(){
    var _=this;
    var handler=function(func){  func(_);}
    if(this.tabPages.getCount()==0){return;}
    if(this.currentTabPage==null){
      this.currentTabPage=this.tabPages.items[this.defaultPage];
    }
    this.onInit.each(handler);
    if($(this.preButton)) $(this.preButton).onclick=this.GetFunction(this,"pre");
    if($(this.nextButton)) $(this.nextButton).onclick=this.GetFunction(this,"next");
  },
  defaultInitHandler:function(obj){
    var handler=function(item){ V.addListener(item.trigger,obj.triggerType,obj.selectHanlder,obj);O.hide(item.sheet); };
    obj.tabPages.each(handler);
    obj.select(obj.defaultPage);
  },
  autoSlideInitHandler:function(o){
    if(!o.enableSlide){return;}
    var delayStartEv=null;
    var delayStartHandler=function(){
      delayStartEv=setTimeout(function(){o.autoSlideHandler(o);},300);
    };
 
    var clearHandler=function(){
      clearTimeout(delayStartEv);
      clearInterval(o._autoSlideEv);
    };
 
    var handler=function(item){
      V.addListener(item.trigger,o.triggerType,clearHandler,o); 
      V.addListener(item.sheet,'mouseover',clearHandler,o); 
      V.addListener([item.trigger,item.sheet],'mouseout',delayStartHandler,o);
    };
 
    o.tabPages.each(handler);
    o.autoSlideHandler(o);
  },
 
  autoSlideHandler:function(o){
    var count=o.tabPages.getCount();
    clearInterval(o._autoSlideEv);
    o._autoSlideEv=setInterval(function(){
      var i=o.indexOf(o.currentTabPage.trigger);
      if(i==-1){return;}
      i++;
      if(i>=count){i=0;}
      o.select(i);
    },o.slideInterval);
  },
  selectHanlder:function(e,o){
    var i= this.indexOf(o);
    this.select(i);
  },
 
  select:function(i){
    var page=null;
    if(this.autoLoop) else if(i>=this.tabPages.getCount()){
        page=this.tabPages.items[0];
      } else {
        page=this.tabPages.items[i];
      }
    } else {
      if(i<0||i>=this.tabPages.getCount()){return;}
      page=this.tabPages.items[i];
    }
    var _=this;
    var handler=function(func){ func(_.currentTabPage,page);};
    this.onChanging.each(handler);
    this.currentTabPage=page;
 
    //设置上一张下一张按钮状态
 
    if($(this.preButton)){
      $(this.preButton).className="enable";
      if(i==0) 
        $(this.preButton).className="unenable";
    }
    if($(this.nextButton)){
    $(this.nextButton).className="enable";
      if(i==this.tabPages.getCount()-1) 
        $(this.nextButton).className="unenable";
    }
 
    if(typeof(this.onComplete)=="function"){
      this.onComplete(this.options,i,this.currentTabPage);
    }   
 
  },
    defaultChangingHandler:function(oldTabPage,newTabPage){
       if(oldTabPage.sheet){
      O.hide(oldTabPage.sheet);
    }
    if(newTabPage.sheet){
      O.show(newTabPage.sheet);
    }
    O.removeClass(oldTabPage.trigger,'current');
    O.addClass(newTabPage.trigger,'current');
     },
  indexOf:function(trigger){
    var r=-1;
    var handler=function(item){return item.trigger==trigger;};
    var item=this.tabPages.find( handler );
    if(item!=null){
      r=this.tabPages.indexOf(item);
    }
    return r;
  },
  GetFunction:function(variable,method,param){
    return function(){
      variable[method](param);
    }
  }
} 
var BX={
	version:'1.0.10.A',
	encoding:'utf-8',
	author:'bigtreexu'
};
BX.namespace=function(ns){
	if(!ns||!ns.length)
	{
		return null;
	}
	var _pr=ns.split('.');
	var _nx=BX;
	for(var i=0;i!=_pr.length;i++)
	{		_nx[_pr[i]]=_nx[_pr[i]]||{};
		_nx=_nx[_pr[i]];
	}
 
}
 
function $(el)
{
	if(!el)
	{
		return null;
	}
	else if(typeof el=='string')
	{
		return document.getElementById(el);
	}
	else if(typeof el=='object')
	{
		return el;
	}
}
 
 
function $A(els){
	var _els=[];
	if(els instanceof Array)
	{
		for(var i=0;i!=els.length;i++)
		{
			_els[_els.length]=$(els[i]);
		}
	}
	else if(typeof els=='object'&&typeof els['length']!='undefined'&&els['length']>0)
	{
		for(var i=0;i!=els.length;i++)
		{
			_els[_els.length]=$(els[i]);
		}
	}else
	{
		_els[0]=$(els);
	}
	return _els;
}
 
BX.Dom={
	_batch:function(el,func)
	{
		var _el=$A(el);
		for(var i=0;i!=_el.length;i++)
		{
			if(_el[i])
			{
				func(_el[i]);
			}
		}
	},	
 
	hide:function(els)
	{
		var _run=function(el)
		{
			el.style.display='none';
		}
		this._batch(els,_run);
	},
	show:function(els)
	{
		var _run=function(el)
		{
			el.style.display='block';
		}
		this._batch(els,_run);
	},
	addClass:function(els,val)
	{
		if(!val)
		{
			return;
		}
		var _run=function(el)
		{
			var _cln=el.className.split(' ');
			for(var i=0;i!=_cln.length;i++)
			{
				if(_cln[i]==val)
				{
					return;
				}
			}
			if(el.className.length>0)
			{
				el.className=el.className+' '+val;
			}
			else
			{
				el.className=val;
			}
		}
		this._batch(els,_run);
	},
	
	removeClass:function(els,val)
	{
		if(!val)
		{
			return;
		}
		var _run=function(el)
		{
			var _cln=el.className.split(' ');
			var _s='';
			for(var i=0;i!=_cln.length;i++)
			{
				if(_cln[i]!=val)
				{
					_s+=_cln[i]+' ';
				}
			}
			if(_s==' ')
			{
				_s='';
			}
			if(_s.length!=0)
			{
				_s=_s.substr(0,_s.length-1);
			}
			el.className=_s;
		}
		this._batch(els,_run);
	}
}
 
BX.Event={
	_cache:[],
	_batch:function(els,func)
	{
		try{
			els=$A(els);
			for(var i=0;i<els.length;i++){
				func(els[i]);
			}
		}
		catch(e)
		{	
 
		}
	},
 
	addListener:function(els,eventName,func,range){
		var _run=function(el){
			var _scope=el;
			var _fn=function(e){
				var _ev=e||window.event;
				//传递相应事件的元素对象
				if(range){
					func.apply(range,[_ev,_scope])
				}
				else
				{
					func(_ev,_scope);
				}
			};
			if (!BX.Event._cache[el])
			{
				BX.Event._cache[el]=[];
			}
			/*防止重复绑定同样的事件*/
 
			if (BX.Event._cache[el][func]) 
			{
				//return false;
			}
			BX.Event._cache[el][func]=_fn;
			if(el.attachEvent){
				el.attachEvent('on'+eventName,_fn);
			}else if(el.addEventListener){
				el.addEventListener(eventName,_fn,false);
			}
			else
			{
				el['on'+eventName] = _fn;
			}
		};
		this._batch(els,_run);
	},
	removeListener:function(els,eventName,func)
  {
    var _run=function(el){
      if(el.detachEvent){
        el.detachEvent('on'+eventName,BX.Event._cache[el][func]);
      }else if(el.removeEventListener){
        el.removeEventListener(eventName,BX.Event._cache[el][func],false);
      }else{
        el['on'+eventName] = null;
      }
      BX.Event._cache[el][func]=null;
    }
    this._batch(els,_run);
  }
}
 
 
 
BX.isEditMode=function(){var r=0;var h=location.pathname.toLowerCase();if(h.indexOf('.aspx')!=-1){r=1;}return r;}();
 
var O=BX.Dom;
var V=BX.Event;
var F=BX.Effect;
var C=BX.Cookie;
 
 
function getElementsByClassName(parentElement,tagName,className){
	var elements=parentElement.getElementsByTagName(tagName);
	var result=[];
	for(var i=0;i<elements.length;i++)
		if((" "+elements[i].className+" ").indexOf(" "+className+" ")!=-1) result.push(elements[i]);
	return result;
}
 
</script> 
 
 
 
<script type="text/javascript"> 
function getElementsByClassName(parentElement,tagName,className){
  var elements=parentElement.getElementsByTagName(tagName);
  var result=[];
  for(var i=0;i<elements.length;i++)
    if((" "+elements[i].className+" ").indexOf(" "+className+" ")!=-1) result.push(elements[i]);
  return result;
}
var tabControlSlide=new TabControl();
        tabControlSlide.addRange($("controlSlide").getElementsByTagName("li"),getElementsByClassName($("tabSlide"),"div","bigPic03")); 
        tabControlSlide.preButton='prevSlide';
        tabControlSlide.nextButton='nextSlide';
        tabControlSlide.autoLoop= true;
        tabControlSlide.triggerType='mouseover';
        tabControlSlide.enableSlide=true;
        tabControlSlide.slideInterval=5000;
        tabControlSlide.init();
        $("prevSlide").onmouseover=function(){this.getElementsByTagName("div")[0].setAttribute("style","filter:alpha(opacity=100);-moz-opacity:1;opacity:1;")}; 
        $("nextSlide").onmouseover=function(){this.getElementsByTagName("div")[0].setAttribute("style","filter:alpha(opacity=100);-moz-opacity:1;opacity:1;")};
        $("prevSlide").onmouseout=function(){this.getElementsByTagName("div")[0].setAttribute("style","filter:alpha(opacity=60);-moz-opacity:0.6;opacity:0.6;")};
        $("nextSlide").onmouseout=function(){this.getElementsByTagName("div")[0].setAttribute("style","filter:alpha(opacity=60);-moz-opacity:0.6;opacity:0.6;")};
</script>

</body></html>