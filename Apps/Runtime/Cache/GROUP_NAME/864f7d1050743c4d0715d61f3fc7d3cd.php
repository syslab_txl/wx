<?php if (!defined('THINK_PATH')) exit();?>﻿<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title><?php echo ($title); ?>-<?php echo ($sitename); ?></title>
<meta name="keywords" content="<?php echo ($keywords); ?>" />
<meta name="description" content="<?php echo ($description); ?>" />
<link href="../Public/css/style.css" rel="stylesheet" type="text/css" />
<link href="../Public/css/validator.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="../Public/css/style_shouye.css"/>
<script type="text/javascript" src="../Public/js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="../Public/js/jquery.form.js"></script>
<script type="text/javascript" src="../Public/js/formValidator-4.0.1.min.js"></script>
<script type="text/javascript" src="../Public/js/formValidatorRegex.js"></script>
<script type="text/javascript">
    //指定当前组模块URL地址 
    var URL = '__URL__';
    var APP	 = '__APP__';
    var PUBLIC='__PUBLIC__';
    var Public = '../Public/';

    $(function(){
        // 导航特效
        var _obj = $(".nav");
        if(_obj && _obj.length > 0){
                $("ul > li:has(ul)",_obj).hover(function(){
                        $(this).children("ul").stop(true,true).slideDown(400);
                },function(){
                        $(this).children("ul").stop(true,true).slideUp("fast");
                });
        }

        // 跑马灯 marquee
        $("#marquee").effectBlockSlide({speed:3000});
        
    });
    

    // 幻灯片特效插件
    (function($){
	$.fn.effectBlockSlide = function(options){
	  var defaults = {
		  speed: "3000"
	  };

	  var newOptions = $.extend(defaults,options);
	  return this.each(function(){

		  var obj = $(this);
		  var length = obj.children().size();
		  obj.children().not(":first").hide();
		  
		  var intervalId;
		  
		  function slideInterval(){

                            intervalId = setInterval(function(){

                                      obj.children().eq(0).slideUp();
                                          obj.children().eq(1).slideDown("normal",function(){
                                              obj.append(obj.children().eq(0).detach());

                                          }); 

                           },newOptions.speed);  
		  }
		  
		  if(length > 1){slideInterval()};
                    obj.mouseover(function(){
                            window.clearInterval(intervalId);
                    });
                    obj.mouseout(function(){
                            if(length > 1){slideInterval()};
                    });   
	  });
	};
})(jQuery);

</script>
</head>
<style type="text/css">
#nav_<?php echo ($position[0]['id']); ?>{border-bottom: 6px solid #4cc8b5;}
</style>
<body style="background-color:#f4f4f4;">
<?php $other=$_result=M('Other')->where('status=1 and settag="lastatistics"')->find(); echo $other['setvalue'];?>
<?php $other=$_result=M('Other')->where('status=1 and settag="GoogleAnalytics "')->find(); echo $other['setvalue'];?>
<?php $other=$_result=M('Other')->where('status=1 and settag="bdtongji"')->find(); echo $other['setvalue'];?>
<?php $other=$_result=M('Other')->where('status=1 and settag="bdshare"')->find(); echo $other['setvalue'];?>
<!--定义变量-->
<?php $nopicpath = '../Public/theme/nopic.gif'; ?>

<div class="header">
        <a href="__APP__" class="logo">
           <img src="__ROOT__/Uploads<?php $set=$_result=M('Set')->getField('logo',1); echo $set;?>" alt="微信公众账号导航" width="320" height="54"/>
        </a>

        <div style="position: absolute;left: 855px;">
            <?php $other=$_result=M('Other')->where('status=1 and settag="homepage"')->find(); echo $other['setvalue'];?><span> | </span>
            <?php $other=$_result=M('Other')->where('status=1 and settag="collect"')->find(); echo $other['setvalue'];?>
        </div>

        <div class="liveproduct">
            <?php if($_SESSION['YFIndex_']['account']!= null): ?><p>
                    <a>欢迎您！</a><a href="<?php echo U('Member/index');?>" title="<?php echo (session('account')); ?>&nbsp;个人中心"><b><?php echo (session('account')); ?></b></a><span>|</span>
                     <a href="<?php echo U('Member/add');?>"  onclick="_hmt.push(['_trackPageview', '/index.php/weixin/add']);" target="_blank">申请收录</a><span>|</span><a href="<?php echo U('Member/logout');?>">退出</a>
                </p>
            <?php else: ?>
                <p>
                     <a href="<?php echo U('Weixin/add',array('id'=>91));?>" onclick="_hmt.push(['_trackPageview', '/index.php/weixin/add']);" target="_blank">申请收录</a><span>|</span><a href="<?php echo U('Member/register');?>" onclick="_hmt.push(['_trackPageview', '/index.php/member/register']);" target=" _blank">用户注册</a><span>|</span><a href="<?php echo U('Member/login');?>" onclick="_hmt.push(['_trackPageview', '/index.php/member/login']);" target="_blank">用户登陆</a>
                </p><?php endif; ?>
        </div>
        <!--
        <div class="member">
            <?php if($_SESSION['YFIndex_']['account']!= null): ?>欢迎您！<a href="<?php echo U('Member/index');?>" title="<?php echo (session('account')); ?>&nbsp;个人中心"><b><?php echo (session('account')); ?></b></a>&nbsp;&nbsp;<a href="<?php echo U('Member/logout');?>" target='_top'>退出</a>
                &nbsp;<span style="color:#18A206;">|</span>&nbsp;<a href="<?php echo U('Member/add');?>">提交微信公号</a>
                <?php else: ?>
                <a href="<?php echo U('Member/register');?>">注册</a>&nbsp;<span style="color:#18A206;">|</span>&nbsp;<a href="<?php echo U('Member/login');?>">登录</a>
                <?php if(($isaddaccount) == "1"): ?>&nbsp;<span style="color:#18A206;">|</span>&nbsp;<a href="<?php echo U('Weixin/add',array('id'=>91));?>">提交微信公号</a>
                <?php else: ?>
                &nbsp;<span style="color:#18A206;">|</span>&nbsp;<a href="<?php echo U('Member/login');?>">提交微信公号</a><?php endif; endif; ?>
            
        </div>-->
    
</div>
<div class="nav_bg">
    <div class="nav">
        <ul>
            <li><a href="/" id="nav_0"><span>首页</span></a></li>
            <?php if(is_array($nav_list)): $i = 0; $__LIST__ = $nav_list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li>
                <?php if(empty($vo["sub_nav"])): ?><a href="<?php echo ($vo["url"]); ?>" id="nav_<?php echo ($vo["id"]); ?>"><span><?php echo ($vo["catname"]); ?></span></a>
                    <?php else: ?>
                    <a href="<?php echo ($vo["url"]); ?>" id="nav_<?php echo ($vo["id"]); ?>" <?php if(($position[0]['id']) == $vo["id"]): ?>class="parent current"<?php else: ?>class="parent"<?php endif; ?> ><span><?php echo ($vo["catname"]); ?></span></a>
                    <ul style="z-index:1000;" class="sub_nav_ul">
                        <?php if(is_array($vo["sub_nav"])): $i = 0; $__LIST__ = $vo["sub_nav"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$sub): $mod = ($i % 2 );++$i;?><li <?php if(($key) == "0"): ?>class="first"<?php endif; ?>><a href="<?php echo ($sub["url"]); ?>"  class="catalog"><?php echo ($sub["catname"]); ?></a>
                                <?php if(is_array($sub["sub_sub_nav"])): $i = 0; $__LIST__ = $sub["sub_sub_nav"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$sub_sub): $mod = ($i % 2 );++$i;?><a href="<?php echo ($sub_sub["url"]); ?>" <?php  if($sub_sub['catname']=="模特"||$sub_sub['catname']=="科技") echo 'style="color:#FF33CC;"'; ?>><?php echo ($sub_sub["catname"]); ?></a><?php endforeach; endif; else: echo "" ;endif; ?>                   
                            </li><?php endforeach; endif; else: echo "" ;endif; ?>
                    </ul><?php endif; ?>
            </li><?php endforeach; endif; else: echo "" ;endif; ?>
             
        </ul>
        <div class="search">
            <form action="<?php echo U('Weixin/search',array('id'=>126));?>" method="post">
                <input type="text" name='search' id='search' title="请输入关键字(公众账号/微信账号)"  value="<?php echo (urldecode($text)); ?>">
                <input type="submit" class="button" value='' id="button" >
            </form>
        </div>
    </div>
</div>
<div class="subinfo" style="width:950px;">
    <div class="announce">公告：</div>
        <div class="marquee">
         <ul id="marquee">
            <?php $_result=M('Announce')->where('status=1 and endtime >= "2014-02-16"')->field('id,title')->order('create_time desc')->limit(5)->select();foreach($_result as $key=>$announce):?><li><a href="<?php echo U('Announce/show',array('id'=>$announce['id']));?>" target=_blank><?php echo (msubstr($announce["title"],0,18)); ?></a></li><?php endforeach;?>
         </ul>
        </div>
        <div class="qq">
            <?php $other=$_result=M('Other')->where('status=1 and settag="qqqun"')->find(); echo $other['setvalue'];?>
        </div>
</div>


<div class="focus">
	<div class="flash">
		<iframe marginheight="0" marginwidth="0" border="0" src="__APP__/Slide" frameborder="no" height="260" scrolling="no" width="690"></iframe>
	</div>

	<div class="newhua">
		<div class="recommend_enter_block mt30">
			<p class="recommend_title">每日一荐</p>
		<?php $_result=M('tuijian')->join('wx_weixin ON wx_weixin.id=wx_tuijian.wxid')->where('wx_weixin.status=1 and ((starttime<=1392529195 and endtime>=1392442795) or timelimit=0) and wx_tuijian.status=1 and recommendid=4 and wx_weixin.catid in (53,56,57,58,59,60,61,62,63,64,65,94,1,66,67,68,69,70,71,72,73,74,75,92,44,112,113,114,115,116,117,125,50,107,108,109,110,111,124,49,76,77,78,79,80,81,82,83,84,85,93,48,95,96,97,98,99,100,101,102,103,104,105,106,123,47,118,119,120,121,122,86)')->field('wx_weixin.*')->order('timelimit desc,wx_tuijian.create_time desc')->limit(1)->select();foreach($_result as $key=>$tuijian):?><div id="recoPic" style="padding:10px;float:left;width:125px;">
				<a href="<?php echo U('Weixin/show',array('id'=>$tuijian['id']));?>" target="_blank">
					<img src="<?php if(empty($tuijian["logo"])): if(empty($tuijian["weblogo"])): ?>../Public/images/nopic.gif<?php else: echo ($tuijian["weblogo"]); endif; else: ?>__ROOT__/Uploads<?php echo ($tuijian["logo"]); endif; ?>" width="125" height="125"/>
					<img src="__ROOT__/Uploads<?php echo ($tuijian["qrcode"]); ?>" style="display:none;" width="125" height="125"/>
				</a>
			</div>
			<div style="font-size: 15px;font-weight: bold;margin-top: 10px;">
				<a style="color: #666;margin-left:15px;text-decoration: none;" href="<?php echo U('Weixin/show',array('id'=>$tuijian['id']));?>"  target="_blank"><?php echo (msubstr($tuijian["pubaccount"],0,6)); ?></a>
			</div>
			<div style="margin-top:5px;">
				<span style="margin-left:15px;font-size:18px;">
					<?php
 switch ($tuijian["xingji"]) { case 1: echo "★☆☆☆☆"; break; case 2: echo "★★☆☆☆"; break; case 3: echo "★★★☆☆"; break; case 4: echo "★★★★☆"; break; case 5: echo "★★★★★"; break; } ?>
				</span>
			</div>
			<div style="margin-top:5px;">
				<span style="margin-left:15px;font-size:15px;">关注<?php echo ($tuijian["hits"]); ?>次</span>
			</div>
			<div class="guanzhu_shouye">
				<a href="<?php echo U('Weixin/show',array('id'=>$tuijian['id']));?>" style="color:white;" target="_blank">加关注</a>
			</div>
			<div class="desc">
				<p><?php echo (msubstr($tuijian["content"],0,55)); ?></p>
			</div><?php endforeach;?>
		<div class="sitead"></div>
	</div>
</div>

</div>

<script type="text/javascript">
	$(document).ready(function(){
		$(".sub_wrap_why ul li").unbind();
		$(".pop").unbind();
		$(".con-prompt").mouseover(function(){
			//$(this).children().children().eq(0).hide();
			//$(this).children().children().eq(1).show();
			$(this).next().css('left',$(this).width()).show();
		}).mouseout(function(){
			//$(this).children().children().eq(0).show();
			//$(this).children().children().eq(1).hide();
			$(this).next().hide();
		});

		$("#recoPic").mouseover(function(){
			$(this).children().eq(0).children().eq(0).hide();
			$(this).children().eq(0).children().eq(1).show();
		}).mouseout(function(){
			$(this).children().eq(0).children().eq(0).show();
			$(this).children().eq(0).children().eq(1).hide();
		});
	});

	function swap(obj1,obj2,direction)
	{
		if(obj1.attr("class")!="sub_wrap_why"||obj2.attr("class")!="sub_wrap_why")
			return;
		if(obj1==null||obj2==null)
			return;
		if(direction=="left"&&obj1.css("display")=="none")
			return;
		if(direction=="right"&&obj2.css("display")=="none")
			return;
		if(direction=="left")
		{
			obj2.css("left",obj1.offset().left+obj1.parent().width());
			obj2.show();
			obj1.animate({left:"-"+obj1.parent().width()+"px"},700,"linear",function(){
				$(this).hide();
				$(this).css("left","0px");
			});
			obj2.animate({"left":"0"},700);
		}
		else if(direction=="right")
		{
			obj1.css("left","-"+obj2.parent().width()+"px");
			obj1.show();
			obj2.animate({left:obj2.offset().left+obj2.parent().width()+"px"},700,"linear",function(){
				$(this).hide();
				$(this).css("left","0px");
			});
			obj1.animate({"left":"0"},700);
		}
	}

	function findWKNode(_this)
	{
		var wknodes = _this.parent().children(".sub_wrap_why");
		var node = null;
		for(var i=0;i<wknodes.length;i++)
		{
			if($(wknodes[i]).css("display")!="none")
			{
				node = $(wknodes[i]);
				break;
			}
		}
		return node;
	}

	$(document).ready(function(){
		$(".prevWrap").click(function(){
			var node = findWKNode($(this));
			swap(node.prev(),node,"right");
		});

		$(".nextWrap").click(function(){
			var node = findWKNode($(this));
			swap(node,node.next(),"left");
		});
	});
	
</script>

<div class="main_wrap_why">
	
	<div class="zone">
	<div class="l">
	<div class="t" style="position:relative;z-index:100;">
			<h2 title="小编推荐"><a href="#" target="_blank">小编推荐</a></h2>
			</div>
	<div style="width:300px;height:335px;left:-115px;top:inherit;position:absolute;background-color:#f4f4f4;z-index:2;"></div>
	<div style="width:600px;height:335px;left:875px;px;top:inherit;position:absolute;background-color:#f4f4f4;z-index:2;"></div>
	<div class="con">
		<div class="prevWrap" style="top:133px;">
			<a><img style="left: 2px;position: absolute;top: 3px;background: none;border: none;padding: 0px;" src="../Public/source/left.png" height="30" width="30"></a>
		</div>
			<?php
 $db = new Model(); $data = $db->table("wx_tuijian tj,wx_weixin wx")->where("tj.wxid=wx.id and tj.status=1 and tj.recommendid=1 and(tj.endtime=0 or tj.endtime>tj.create_time)")->field("wx.id,wx.content,wx.update_time,wx.logo,wx.qrcode,wx.pubaccount,wx.xingji,wx.hits")->order("xingji desc")->select(); $tuijian = $data; $num = count($tuijian); $page_num = floor($num%12==0?$num/12:($num/12+1)); $last_page = $num%12; for($i=0;$i<$page_num;$i++) { $curNum = 12; if($i>=$page_num-1&&$last_page!=0) $curNum = $last_page; ?>
			<div class="sub_wrap_why" style=<?=$i!=0?"display:none;":""?>>		
				<ul id="con01">
					<?php
 for($n=0;$n<$curNum;$n++) { $index = $i*12+$n; $weixin = $tuijian[$index]; ?>
					<li>
						<div class="con-prompt"> <a href="<?php echo U('Weixin/show',array('id'=>$weixin['id']));?>" target="_blank">

							<img class="appPic" src="<?php if(empty($weixin["logo"])): if(empty($weixin["weblogo"])): ?>../Public/images/nopic.gif<?php else: echo ($weixin["weblogo"]); endif; else: ?>__ROOT__/Uploads<?php echo ($weixin["logo"]); endif; ?>" width="80" height="80">
							<h3><?php echo (msubstr($weixin["pubaccount"],0,7)); ?></h3>
							<p><img src="../Public/source/index_icon05.png" width="12" height="10">						
							<?=date("m-d",$weixin["update_time"])?></p>
							</a>
							<!--<div class="downbtn none" style="display: none;"><span><a href="http://www.zhaogonghao.com/lvyou/21079.html" target="_blank">加关注</a></span></div>-->
						</div>
						<div class="pop">
                                <div class="panel">
                                    <div class="ewm"><img src="<?php if(empty($weixin["logo"])): if(empty($weixin["weblogo"])): ?>../Public/images/nopic.gif<?php else: echo ($weixin["weblogo"]); endif; else: ?>__ROOT__/Uploads<?php echo ($weixin["qrcode"]); endif; ?>"></div>
                                    <div class="title"><?php echo (msubstr($weixin["pubaccount"],0,24)); ?></div>
                                    <div class="star"><img src="../Public/images/xx<?php echo ($tuijian["xingji"]); ?>.jpg" /></div>
                                    <div class="rate"><?php echo ($weixin["hits"]); ?>人关注</div>
                                    <div class="des"><p style="text-indent:0px;"><?php echo (msubstr(strip_tags($weixin["content"]),0,20)); ?></p></div>
                                </div>
                                <div class="arrow">
                                    <div class="arrow1"></div>
                                    <div class="arrow2"></div>
                               </div> 
                        </div>
					</li>
					<?php } ?>
				</ul>
			</div>
			<?php } ?>

		<div class="nextWrap" style="right:0;top:133px;">
			<a><img style="left: 2px;position: absolute;top: 3px;background: none;border: none;padding: 0px;" src="../Public/source/right.png" height="30" width="30"></a>
		</div>
		</div>
	</div>
	</div>

	<div class="r">
		<div class="rt">
			<h2><a href="<?php echo U('Weixin/news',array('id'=>'90'));?>">最新收录</a></h2>
			</div>
		<div class="index-side index-side02"> 			
			<ol>
				<?php $_result=M('Weixin')->where('status=1 and catid in (53,56,57,58,59,60,61,62,63,64,65,94,1,66,67,68,69,70,71,72,73,74,75,92,44,112,113,114,115,116,117,125,50,107,108,109,110,111,124,49,76,77,78,79,80,81,82,83,84,85,93,48,95,96,97,98,99,100,101,102,103,104,105,106,123,47,118,119,120,121,122,86)')->field('id,logo,pubaccount,xingji')->order('create_time desc')->limit(8)->select();foreach($_result as $key=>$weixin):?><li>
					<div class="con-prompt2">
						<code style="width:65px;">
						<?php
 switch ($weixin["xingji"]) { case 1: echo "★☆☆☆☆"; break; case 2: echo "★★☆☆☆"; break; case 3: echo "★★★☆☆"; break; case 4: echo "★★★★☆"; break; case 5: echo "★★★★★"; break; } ?>
						</code>
						<a href="<?php echo U('Weixin/show',array('id'=>$weixin['id']));?>" target="_blank"><img src="<?php if(empty($weixin["logo"])): if(empty($weixin["weblogo"])): ?>../Public/images/nopic.gif<?php else: echo ($weixin["weblogo"]); endif; else: ?>__ROOT__/Uploads<?php echo ($weixin["logo"]); endif; ?>" width="20" height="20"></a> <a href="<?php echo U('Weixin/show',array('id'=>$weixin['id']));?>" style="margin-left:5px;" target="_blank"><?php echo (msubstr($weixin["pubaccount"],0,13)); ?></a>
					</div>
				</li><?php endforeach;?>	
			</ol>
		</div>
	</div>

	<div class="zone">
	<div class="l">
	<div class="t">
			<h2 title="订阅号精选"><a href="#" target="_blank">订阅号精选</a></h2>
			</div>
	<div style="width:300px;height:335px;left:-115px;top:inherit;position:absolute;background-color:#f4f4f4;z-index:2;"></div>
	<div style="width:600px;height:335px;left:875px;px;top:inherit;position:absolute;background-color:#f4f4f4;z-index:2;"></div>
	<div class="con">
		<div class="prevWrap" style="top:133px;">
			<a><img style="left: 2px;position: absolute;top: 3px;background: none;border: none;padding: 0px;" src="../Public/source/left.png" height="30" width="30"></a>
		</div>
		<?php
 $db = new Model(); $data = $db->table("wx_tuijian tj,wx_weixin wx")->where("tj.wxid=wx.id and tj.status=1 and tj.recommendid=5 and(tj.endtime=0 or tj.endtime>tj.create_time)")->field("wx.id,wx.update_time,wx.logo,wx.qrcode,wx.pubaccount,wx.xingji,wx.hits")->order("xingji desc")->select(); $tuijian = $data; $num = count($tuijian); $page_num = floor($num%12==0?$num/12:($num/12+1)); $last_page = $num%12; for($i=0;$i<$page_num;$i++) { $curNum = 12; if($last_page!=0&&$i>=$page_num-1) $curNum = $last_page; ?>
			<div class="sub_wrap_why" style=<?=$i!=0?"display:none;":""?>>		
				<ul id="con01">
					<?php
 for($n=0;$n<$curNum;$n++) { $index = $i*12+$n; $weixin = $tuijian[$index]; ?>
					<li>
						<div class="con-prompt"> <a href="<?php echo U('Weixin/show',array('id'=>$weixin['id']));?>" target="_blank">

							<img class="appPic" src="<?php if(empty($weixin["logo"])): if(empty($weixin["weblogo"])): ?>../Public/images/nopic.gif<?php else: echo ($weixin["weblogo"]); endif; else: ?>__ROOT__/Uploads<?php echo ($weixin["logo"]); endif; ?>" width="80" height="80">

							<img class="appQRCode" src="<?php if(empty($weixin["logo"])): if(empty($weixin["weblogo"])): ?>../Public/images/nopic.gif<?php else: echo ($weixin["weblogo"]); endif; else: ?>__ROOT__/Uploads<?php echo ($weixin["qrcode"]); endif; ?>" style="display:none;" width="80" height="80">
							<h3><?php echo (msubstr($weixin["pubaccount"],0,7)); ?></h3>
							<p><img src="../Public/source/index_icon05.png" width="12" height="10">						
							<?=date("m-d",$weixin["update_time"])?></p>
							</a>
							<!--<div class="downbtn none" style="display: none;"><span><a href="http://www.zhaogonghao.com/lvyou/21079.html" target="_blank">加关注</a></span></div>-->
						</div>
						<div class="pop">
                                <div class="panel">
                                    <div class="ewm"><img src="<?php if(empty($weixin["logo"])): if(empty($weixin["weblogo"])): ?>../Public/images/nopic.gif<?php else: echo ($weixin["weblogo"]); endif; else: ?>__ROOT__/Uploads<?php echo ($weixin["qrcode"]); endif; ?>"></div>
                                    <div class="title"><?php echo (msubstr($weixin["pubaccount"],0,24)); ?></div>
                                    <div class="star"><img src="../Public/images/xx<?php echo ($tuijian["xingji"]); ?>.jpg" /></div>
                                    <div class="rate"><?php echo ($weixin["hits"]); ?>人关注</div>
                                    <div class="des"><p style="text-indent:0px;"><?php echo (msubstr(strip_tags($weixin["content"]),0,20)); ?></p></div>
                                </div>
                                <div class="arrow">
                                    <div class="arrow1"></div>
                                    <div class="arrow2"></div>
                               </div> 
                        </div>
					</li>
					<?php } ?>
				</ul>
			</div>
			<?php } ?>
		<div class="nextWrap" style="right:0;top:133px;">
			<a><img style="left: 2px;position: absolute;top: 3px;background: none;border: none;padding: 0px;" src="../Public/source/right.png" height="30" width="30"></a>
		</div>
		</div>
	</div>
	</div>

	<div class="r">
		<div class="rt">
			<h2>关注排行</h2>
			</div>
		<div class="index-side index-side02"> 			
			<ol>
				<?php $_result=M('Weixin')->where('status=1 and catid in (53,56,57,58,59,60,61,62,63,64,65,94,1,66,67,68,69,70,71,72,73,74,75,92,44,112,113,114,115,116,117,125,50,107,108,109,110,111,124,49,76,77,78,79,80,81,82,83,84,85,93,48,95,96,97,98,99,100,101,102,103,104,105,106,123,47,118,119,120,121,122,86)')->field('id,logo,pubaccount,xingji,hits')->order('hits desc')->limit(8)->select();foreach($_result as $key=>$weixin):?><li>
					<div class="con-prompt2">
						<code style="width:65px;">
						<?php
 switch ($weixin["xingji"]) { case 1: echo "★☆☆☆☆"; break; case 2: echo "★★☆☆☆"; break; case 3: echo "★★★☆☆"; break; case 4: echo "★★★★☆"; break; case 5: echo "★★★★★"; break; } ?>
						</code>
						<a href="<?php echo U('Weixin/show',array('id'=>$weixin['id']));?>" target="_blank"><img src="<?php if(empty($weixin["logo"])): if(empty($weixin["weblogo"])): ?>../Public/images/nopic.gif<?php else: echo ($weixin["weblogo"]); endif; else: ?>__ROOT__/Uploads<?php echo ($weixin["logo"]); endif; ?>" width="20" height="20"></a> <a href="<?php echo U('Weixin/show',array('id'=>$weixin['id']));?>" style="margin-left:5px;" target="_blank"><?php echo (msubstr($weixin["pubaccount"],0,13)); ?></a>
					</div>
				</li><?php endforeach;?>			
			</ol>
		</div>
	</div>

	<div class="zone">
	<div class="l">
	<div class="t">
			<h2 title="服务号精选"><a href="#" target="_blank">服务号精选</a></h2>
			</div>
	<div style="width:300px;height:335px;left:-115px;top:inherit;position:absolute;background-color:#f4f4f4;z-index:2;"></div>
	<div style="width:600px;height:335px;left:875px;px;top:inherit;position:absolute;background-color:#f4f4f4;z-index:2;"></div>
	<div class="con">
		<div class="prevWrap" style="top:133px;">
			<a><img style="left: 2px;position: absolute;top: 3px;background: none;border: none;padding: 0px;" src="../Public/source/left.png" height="30" width="30"></a>
		</div>
		<?php
 $db = new Model(); $data = $db->table("wx_tuijian tj,wx_weixin wx")->where("tj.wxid=wx.id and tj.status=1 and tj.recommendid=6 and(tj.endtime=0 or tj.endtime>tj.create_time)")->field("wx.id,wx.update_time,wx.logo,wx.qrcode,wx.pubaccount,wx.xingji,wx.hits")->order("xingji desc")->select(); $tuijian = $data; $num = count($tuijian); $page_num = floor($num%12==0?$num/12:($num/12+1)); $last_page = $num%12; for($i=0;$i<$page_num;$i++) { $curNum = 12; if($i>=$page_num-1&&$last_page!=0) $curNum = $last_page; ?>
			<div class="sub_wrap_why" style=<?=$i!=0?"display:none;":""?>>		
				<ul id="con01">
					<?php
 for($n=0;$n<$curNum;$n++) { $index = $i*12+$n; $weixin = $tuijian[$index]; ?>
					<li>
						<div class="con-prompt"> <a href="<?php echo U('Weixin/show',array('id'=>$weixin['id']));?>" target="_blank">

							<img class="appPic" src="<?php if(empty($weixin["logo"])): if(empty($weixin["weblogo"])): ?>../Public/images/nopic.gif<?php else: echo ($weixin["weblogo"]); endif; else: ?>__ROOT__/Uploads<?php echo ($weixin["logo"]); endif; ?>" width="80" height="80">

							<img class="appQRCode" src="<?php if(empty($weixin["logo"])): if(empty($weixin["weblogo"])): ?>../Public/images/nopic.gif<?php else: echo ($weixin["weblogo"]); endif; else: ?>__ROOT__/Uploads<?php echo ($weixin["qrcode"]); endif; ?>" style="display:none;" width="80" height="80">
							<h3><?php echo (msubstr($weixin["pubaccount"],0,7)); ?></h3>
							<p><img src="../Public/source/index_icon05.png" width="12" height="10">						
							<?=date("m-d",$weixin["update_time"])?></p>
							</a>
							<!--<div class="downbtn none" style="display: none;"><span><a href="http://www.zhaogonghao.com/lvyou/21079.html" target="_blank">加关注</a></span></div>-->
						</div>
						<div class="pop">
                                <div class="panel">
                                    <div class="ewm"><img src="<?php if(empty($weixin["logo"])): if(empty($weixin["weblogo"])): ?>../Public/images/nopic.gif<?php else: echo ($weixin["weblogo"]); endif; else: ?>__ROOT__/Uploads<?php echo ($weixin["qrcode"]); endif; ?>"></div>
                                    <div class="title"><?php echo (msubstr($weixin["pubaccount"],0,24)); ?></div>
                                    <div class="star"><img src="../Public/images/xx<?php echo ($tuijian["xingji"]); ?>.jpg" /></div>
                                    <div class="rate"><?php echo ($weixin["hits"]); ?>人关注</div>
                                    <div class="des"><p style="text-indent:0px;"><?php echo (msubstr(strip_tags($weixin["content"]),0,20)); ?></p></div>
                                </div>
                                <div class="arrow">
                                    <div class="arrow1"></div>
                                    <div class="arrow2"></div>
                               </div> 
                        </div>
					</li>
					<?php } ?>
				</ul>
			</div>
			<?php } ?>
		<div class="nextWrap" style="right:0;top:133px;">
			<a><img style="left: 2px;position: absolute;top: 3px;background: none;border: none;padding: 0px;" src="../Public/source/right.png" height="30" width="30"></a>
		</div>
		</div>
	</div>
	</div>

	<div class="zone">
	<div class="l">
	<div class="t">
			<h2 title="新锐榜"><a href="#" target="_blank">新锐榜</a></h2>
			</div>
	<div style="width:300px;height:335px;left:-115px;top:inherit;position:absolute;background-color:#f4f4f4;z-index:2;"></div>
	<div style="width:600px;height:335px;left:875px;px;top:inherit;position:absolute;background-color:#f4f4f4;z-index:2;"></div>
	<div class="con">
		<div class="prevWrap" style="top:133px;">
			<a><img style="left: 2px;position: absolute;top: 3px;background: none;border: none;padding: 0px;" src="../Public/source/left.png" height="30" width="30"></a>
		</div>
		<?php
 $db = new Model(); $data = $db->table("wx_tuijian tj,wx_weixin wx")->where("tj.wxid=wx.id and tj.status=1 and tj.recommendid=7 and(tj.endtime=0 or tj.endtime>tj.create_time)")->field("wx.id,wx.update_time,wx.logo,wx.qrcode,wx.pubaccount,wx.xingji,wx.hits")->order("xingji desc")->select(); $tuijian = $data; $num = count($tuijian); $page_num = floor($num%12==0?$num/12:($num/12+1)); $last_page = $num%12; for($i=0;$i<$page_num;$i++) { $curNum = 12; if($i>=$page_num-1&&$last_page!=0) $curNum = $last_page; ?>
			<div class="sub_wrap_why" style=<?=$i!=0?"display:none;":""?>>		
				<ul id="con01">
					<?php
 for($n=0;$n<$curNum;$n++) { $index = $i*12+$n; $weixin = $tuijian[$index]; ?>
					<li>
						<div class="con-prompt"> <a href="<?php echo U('Weixin/show',array('id'=>$weixin['id']));?>" target="_blank">

							<img class="appPic" src="<?php if(empty($weixin["logo"])): if(empty($weixin["weblogo"])): ?>../Public/images/nopic.gif<?php else: echo ($weixin["weblogo"]); endif; else: ?>__ROOT__/Uploads<?php echo ($weixin["logo"]); endif; ?>" width="80" height="80">

							<img class="appQRCode" src="<?php if(empty($weixin["logo"])): if(empty($weixin["weblogo"])): ?>../Public/images/nopic.gif<?php else: echo ($weixin["weblogo"]); endif; else: ?>__ROOT__/Uploads<?php echo ($weixin["qrcode"]); endif; ?>" style="display:none;" width="80" height="80">
							<h3><?php echo (msubstr($weixin["pubaccount"],0,7)); ?></h3>
							<p><img src="../Public/source/index_icon05.png" width="12" height="10">						
							<?=date("m-d",$weixin["update_time"])?></p>
							</a>
							<!--<div class="downbtn none" style="display: none;"><span><a href="http://www.zhaogonghao.com/lvyou/21079.html" target="_blank">加关注</a></span></div>-->
						</div>
						<div class="pop">
                                <div class="panel">
                                    <div class="ewm"><img src="<?php if(empty($weixin["logo"])): if(empty($weixin["weblogo"])): ?>../Public/images/nopic.gif<?php else: echo ($weixin["weblogo"]); endif; else: ?>__ROOT__/Uploads<?php echo ($weixin["qrcode"]); endif; ?>"></div>
                                    <div class="title"><?php echo (msubstr($weixin["pubaccount"],0,24)); ?></div>
                                    <div class="star"><img src="../Public/images/xx<?php echo ($tuijian["xingji"]); ?>.jpg" /></div>
                                    <div class="rate"><?php echo ($weixin["hits"]); ?>人关注</div>
                                    <div class="des"><p style="text-indent:0px;"><?php echo (msubstr(strip_tags($weixin["content"]),0,20)); ?></p></div>
                                </div>
                                <div class="arrow">
                                    <div class="arrow1"></div>
                                    <div class="arrow2"></div>
                               </div> 
                        </div>
					</li>
					<?php } ?>
				</ul>
			</div>
			<?php } ?>
		<div class="nextWrap" style="right:0;top:133px;">
			<a><img style="left: 2px;position: absolute;top: 3px;background: none;border: none;padding: 0px;" src="../Public/source/right.png" height="30" width="30"></a>
		</div>
		</div>
	</div>
	</div>

<div style="clear:both;"></div>
</div>

<div class="clearfix"></div>
﻿
<div id="footer_box">

   <div class="link">
        <ul>
            <li><span>友情链接:</span></li>
            <?php if(is_array($flist)): $i = 0; $__LIST__ = $flist;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; if($vo['type'] == 0 ): if($vo['linktype'] == 0): ?><li><a href="<?php echo ($vo["url"]); ?>" target="_blank"><?php echo ($vo["name"]); ?></a</li><?php endif; endif; endforeach; endif; else: echo "" ;endif; ?>
            <li><a href="<?php echo U('Link/index');?>" style="color:red;">更多&gt;</a></li>
        </ul>
    </div>


    <div class="footer_nav">

        <ul>

            <li><a href="__APP__" target="_blank">微信导航</a></li>

            <li><a href="https://mp.weixin.qq.com/cgi-bin/loginpage?t=wxm2-login&lang=zh_CN" target="_blank">微信公众平台</a></li>

            <li><a href="<?php echo U('Member/register');?>" target="_blank">免费注册</a></li>

            <li><a href="#" target="_blank">关于我们</a></li>

        </ul>

    </div>

    <div class="clearfix"></div>

    <div class="footer_right">©2013<?php $other=$_result=M('Other')->where('status=1 and settag="cnzzstatistics"')->find(); echo $other['setvalue'];?>&nbsp;&nbsp;<?php $other=$_result=M('Other')->where('status=1 and settag="lastatistics"')->find(); echo $other['setvalue'];?>  
     </div>

</div>
<script type="text/javascript">
    $(function(){
       // 相册弹出特效
        $("ul.popo li").hover(function(){
                    $(".pop",this).stop(true,true).show().css({"left":$(this).offset().left + $(this).outerWidth()-3 + "px","top":$(this).offset().top + "px"});
                },function(){
                    $(".pop",this).stop(true,true).hide();
        }); 
        $("ul.popo li .pop").mouseover(function(){
            $(this).hide();
        });
        
        // 相册弹出特效
        $("li > dl > dt").hover(function(){
                    $(this).parent().parent().children('.pop').stop(true,true).show().css({"left":$(this).offset().left + $(this).outerWidth()-3 + "px","top":$(this).offset().top + "px"});
                },function(){
                    
                    $(this).parent().parent().children('.pop').stop(true,true).hide();
        }); 
        $("li > .pop").mouseover(function(){
            $(this).hide();
        });
        
        
        // 页面浮动面板
        $("#floatPanel > .ctrolPanel > a.arrow").eq(0).click(function(){$("html,body").animate({scrollTop :0}, 800);return false;});
        $("#floatPanel > .ctrolPanel > a.arrow").eq(1).click(function(){$("html,body").animate({scrollTop : $(document).height()}, 800);return false;});

        var objPopPanel = $("#floatPanel > .popPanel");	
        var w = objPopPanel.outerWidth();
        $("#floatPanel > .ctrolPanel > a.qrcode").bind({
            mouseover : function(){
                        objPopPanel.css("width","0px").show();
                        objPopPanel.animate({"width" : w + "px"},300);return false;
                },
                mouseout : function(){
                        objPopPanel.animate({"width" : "0px"},300);return false;
                        objPopPanel.css("width",w + "px");
            }	
        });
    });
</script>

<!--浮动面板-->
<div id="floatPanel">
     <div class="ctrolPanel">
           <a class="arrow" href="#"><span>顶部</span></a>
           <a class="contact" href="<?php $other=$_result=M('Other')->where('status=1 and settag="feedbackqq"')->find(); echo $other['setvalue'];?>" target="_blank"><span>反馈</span></a>
           <a class="qrcode" href="#"><span>微信二维码</span></a>
           <a class="arrow" href="#"><span>底部</span></a>
     </div>
     <div class="popPanel">
          <div class="popPanel-inner">
                <div class="qrcodePanel">
                     <img src="__UPLOADS__<?php $set=$_result=M('Set')->getField('qrcode',1); echo $set;?>" />
                     <span>扫描二维码关注我为好友</span>
                </div>
                <div class="arrowPanel">
                     <div class="arrow01"></div>
                     <div class="arrow02"></div>
                </div>
          </div>
     </div>
</div>
</body>
</html>
</body></html>