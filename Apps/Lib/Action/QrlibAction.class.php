<?php
	include("./Apps/Lib/phpqrcode/qrlib.php");
	class QrlibAction extends CommonAction
	{
		/**
		*	-------width-------
		*	|           margin
		*	|           ----------
		*	|			|
		*	|           |
		*height---margin|  size of QRCode
		*	|           |
		*	|           |
		*	|
		*	|
		*/
		public function index()
		{
			//import("@.phpqrcode.qrlib.php");
			$_GET['size'] = empty($_GET['size'])?4:$_GET['size'];
			$_GET['margin'] = empty($_GET['margin'])?10:$_GET['margin'];
			QRcode::png($_GET['content'],false,QR_ECLEVEL_M,$_GET['size'],$_GET['margin'],false,$_GET['width'],$_GET['height']);
		}
	}
